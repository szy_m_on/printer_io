/*********************************************************************
	Rhapsody	: 8.3.1
	Login		: student
	Component	: EXE
	Configuration 	: Host
	Model Element	: Printer
//!	Generated Date	: Wed, 15, May 2019 
	File Path	: EXE/Host/Printer/Printer.java
*********************************************************************/

package Printer;

//## auto_generated
import com.ibm.rational.rhapsody.oxf.*;
//## auto_generated
import com.ibm.rational.rhapsody.animation.*;
//## auto_generated
import com.ibm.rational.rhapsody.oxf.states.*;
//## auto_generated
import com.ibm.rational.rhapsody.animcom.animMessages.*;

//----------------------------------------------------------------------------
// Printer/Printer.java                                                                  
//----------------------------------------------------------------------------

//## package Printer 


//## class Printer 
public class Printer implements RiJStateConcept, Animated {
    
    //#[ ignore
    // Instrumentation attributes (Animation)
    private Animate animate;
    
    public static AnimClass animClassPrinter = new AnimClass("Printer.Printer",false);
    //#]
    
    public Reactive reactive;		//## ignore 
    
    protected int adjustTime;		//## attribute adjustTime 
    
    protected int finishTime;		//## attribute finishTime 
    
    protected int printTime;		//## attribute printTime 
    
    protected Display itsDisplay;		//## link itsDisplay 
    
    //#[ ignore 
    public static final int RiJNonState=0;
    public static final int Stop=1;
    public static final int Running=2;
    public static final int Printing=3;
    public static final int LoadFile=4;
    public static final int Finishing=5;
    public static final int Adjusting=6;
    public static final int Off=7;
    //#]
    protected int rootState_subState;		//## ignore 
    
    protected int rootState_active;		//## ignore 
    
    protected int Running_subState;		//## ignore 
    
    protected int Running_lastState;		//## ignore 
    
    public static final int Printer_Timeout_Printing_id = 1;		//## ignore 
    
    public static final int Printer_Timeout_Finishing_id = 2;		//## ignore 
    
    public static final int Printer_Timeout_Adjusting_id = 3;		//## ignore 
    
    
    //## statechart_method 
    public RiJThread getThread() {
        return reactive.getThread();
    }
    
    //## statechart_method 
    public void schedTimeout(long delay, long tmID, RiJStateReactive reactive) {
        getThread().schedTimeout(delay, tmID, reactive);
    }
    
    //## statechart_method 
    public void unschedTimeout(long tmID, RiJStateReactive reactive) {
        getThread().unschedTimeout(tmID, reactive);
    }
    
    //## statechart_method 
    public boolean isIn(int state) {
        return reactive.isIn(state);
    }
    
    //## statechart_method 
    public boolean isCompleted(int state) {
        return reactive.isCompleted(state);
    }
    
    //## statechart_method 
    public RiJEventConsumer getEventConsumer() {
        return (RiJEventConsumer)reactive;
    }
    
    //## statechart_method 
    public void gen(RiJEvent event) {
        reactive._gen(event);
    }
    
    //## statechart_method 
    public void queueEvent(RiJEvent event) {
        reactive.queueEvent(event);
    }
    
    //## statechart_method 
    public int takeEvent(RiJEvent event) {
        return reactive.takeEvent(event);
    }
    
    // Constructors
    
    //## auto_generated 
    public  Printer(RiJThread p_thread) {
        try {
            animInstance().notifyConstructorEntered(animClassPrinter.getUserClass(),
               new ArgData[] {
               });
        
        reactive = new Reactive(p_thread);
        }
        finally {
            animInstance().notifyMethodExit();
        }
        
    }
    
    //## operation message_0() 
    public void message_0() {
        try {
            animInstance().notifyMethodEntered("message_0",
               new ArgData[] {
               });
        
        //#[ operation message_0() 
        //#]
        }
        finally {
            animInstance().notifyMethodExit();
        }
        
    }
    
    //## operation setup() 
    public void setup() {
        try {
            animInstance().notifyMethodEntered("setup",
               new ArgData[] {
               });
        
        //#[ operation setup() 
        adjustTime = 2000;
        printTime = 5000;
        finishTime = 2000;
        //#]
        }
        finally {
            animInstance().notifyMethodExit();
        }
        
    }
    
    //## auto_generated 
    public int getAdjustTime() {
        return adjustTime;
    }
    
    //## auto_generated 
    public void setAdjustTime(int p_adjustTime) {
        adjustTime = p_adjustTime;
    }
    
    //## auto_generated 
    public int getFinishTime() {
        return finishTime;
    }
    
    //## auto_generated 
    public void setFinishTime(int p_finishTime) {
        finishTime = p_finishTime;
    }
    
    //## auto_generated 
    public int getPrintTime() {
        return printTime;
    }
    
    //## auto_generated 
    public void setPrintTime(int p_printTime) {
        printTime = p_printTime;
    }
    
    //## auto_generated 
    public Display getItsDisplay() {
        return itsDisplay;
    }
    
    //## auto_generated 
    public void __setItsDisplay(Display p_Display) {
        itsDisplay = p_Display;
        if(p_Display != null)
            {
                animInstance().notifyRelationAdded("itsDisplay", p_Display);
            }
        else
            {
                animInstance().notifyRelationCleared("itsDisplay");
            }
    }
    
    //## auto_generated 
    public void _setItsDisplay(Display p_Display) {
        if(itsDisplay != null)
            {
                itsDisplay.__setItsPrinter(null);
            }
        __setItsDisplay(p_Display);
    }
    
    //## auto_generated 
    public void setItsDisplay(Display p_Display) {
        if(p_Display != null)
            {
                p_Display._setItsPrinter(this);
            }
        _setItsDisplay(p_Display);
    }
    
    //## auto_generated 
    public void _clearItsDisplay() {
        animInstance().notifyRelationCleared("itsDisplay");
        itsDisplay = null;
    }
    
    //## auto_generated 
    public boolean startBehavior() {
        boolean done = false;
        done = reactive.startBehavior();
        return done;
    }
    
    //## ignore 
    public class Reactive extends RiJStateReactive implements AnimatedReactive {
        
        // Default constructor 
        public Reactive() {
            this(RiJMainThread.instance());
        }
        
        
        // Constructors
        
        public  Reactive(RiJThread p_thread) {
            super(p_thread);
            initStatechart();
        }
        
        //## statechart_method 
        public boolean isIn(int state) {
            if(Running_subState == state)
                {
                    return true;
                }
            if(rootState_subState == state)
                {
                    return true;
                }
            return false;
        }
        
        //## statechart_method 
        public boolean isCompleted(int state) {
            return true;
        }
        
        //## statechart_method 
        public void rootState_add(AnimStates animStates) {
            animStates.add("ROOT");
            switch (rootState_subState) {
                case Running:
                {
                    Running_add(animStates);
                }
                break;
                case Off:
                {
                    Off_add(animStates);
                }
                break;
                case Stop:
                {
                    Stop_add(animStates);
                }
                break;
                default:
                    break;
            }
        }
        
        //## statechart_method 
        public void rootState_entDef() {
            {
                rootState_enter();
                rootStateEntDef();
            }
        }
        
        //## statechart_method 
        public int rootState_dispatchEvent(short id) {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            switch (rootState_active) {
                case Adjusting:
                {
                    res = Adjusting_takeEvent(id);
                }
                break;
                case Printing:
                {
                    res = Printing_takeEvent(id);
                }
                break;
                case Finishing:
                {
                    res = Finishing_takeEvent(id);
                }
                break;
                case LoadFile:
                {
                    res = LoadFile_takeEvent(id);
                }
                break;
                case Off:
                {
                    res = Off_takeEvent(id);
                }
                break;
                case Stop:
                {
                    res = Stop_takeEvent(id);
                }
                break;
                default:
                    break;
            }
            return res;
        }
        
        //## statechart_method 
        public void Stop_add(AnimStates animStates) {
            animStates.add("ROOT.Stop");
        }
        
        //## statechart_method 
        public void Running_add(AnimStates animStates) {
            animStates.add("ROOT.Running");
            switch (Running_subState) {
                case Adjusting:
                {
                    Adjusting_add(animStates);
                }
                break;
                case Printing:
                {
                    Printing_add(animStates);
                }
                break;
                case Finishing:
                {
                    Finishing_add(animStates);
                }
                break;
                case LoadFile:
                {
                    LoadFile_add(animStates);
                }
                break;
                default:
                    break;
            }
        }
        
        //## statechart_method 
        public void Printing_add(AnimStates animStates) {
            animStates.add("ROOT.Running.Printing");
        }
        
        //## statechart_method 
        public void LoadFile_add(AnimStates animStates) {
            animStates.add("ROOT.Running.LoadFile");
        }
        
        //## statechart_method 
        public void Finishing_add(AnimStates animStates) {
            animStates.add("ROOT.Running.Finishing");
        }
        
        //## statechart_method 
        public void Adjusting_add(AnimStates animStates) {
            animStates.add("ROOT.Running.Adjusting");
        }
        
        //## statechart_method 
        public void Off_add(AnimStates animStates) {
            animStates.add("ROOT.Off");
        }
        
        //## auto_generated 
        protected void initStatechart() {
            rootState_subState = RiJNonState;
            rootState_active = RiJNonState;
            Running_subState = RiJNonState;
            Running_lastState = RiJNonState;
        }
        
        //## statechart_method 
        public void Adjusting_entDef() {
            Adjusting_enter();
        }
        
        //## statechart_method 
        public void LoadFile_entDef() {
            LoadFile_enter();
        }
        
        //## statechart_method 
        public void PrintingExit() {
            itsRiJThread.unschedTimeout(Printer_Timeout_Printing_id, this);
        }
        
        //## statechart_method 
        public void Printing_entDef() {
            Printing_enter();
        }
        
        //## statechart_method 
        public int Running_takeEvent(short id) {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            if(event.isTypeOf(evStop.evStop_Printer_id))
                {
                    res = RunningTakeevStop();
                }
            
            return res;
        }
        
        //## statechart_method 
        public void Off_enter() {
            animInstance().notifyStateEntered("ROOT.Off");
            rootState_subState = Off;
            rootState_active = Off;
            OffEnter();
        }
        
        //## statechart_method 
        public void FinishingEnter() {
            //#[ state Running.Finishing.(Entry) 
            System.out.print("\nDisconnecting the toner \n");
            //#]
            itsRiJThread.schedTimeout(finishTime, Printer_Timeout_Finishing_id, this, "ROOT.Running.Finishing");
        }
        
        //## statechart_method 
        public void LoadFileExit() {
        }
        
        //## statechart_method 
        public void StopExit() {
            //#[ state Stop.(Exit) 
            System.out.print("Printing included \n");
            //#]
        }
        
        //## statechart_method 
        public void RunningExit() {
        }
        
        //## statechart_method 
        public void Running_entDef() {
            Running_enter();
            
            animInstance().notifyTransitionStarted("9");
            LoadFile_entDef();
            animInstance().notifyTransitionEnded("9");
        }
        
        //## statechart_method 
        public void Stop_entDef() {
            Stop_enter();
        }
        
        //## statechart_method 
        public void Finishing_exit() {
            FinishingExit();
            animInstance().notifyStateExited("ROOT.Running.Finishing");
        }
        
        //## statechart_method 
        public void RunningEnter() {
        }
        
        //## statechart_method 
        public void Running_entHist() {
            if(Running_lastState != RiJNonState)
                {
                    Running_subState = Running_lastState;
                    switch (Running_subState) {
                        case Adjusting:
                        {
                            Adjusting_enter();
                        }
                        break;
                        case Printing:
                        {
                            Printing_enter();
                        }
                        break;
                        case Finishing:
                        {
                            Finishing_enter();
                        }
                        break;
                        case LoadFile:
                        {
                            LoadFile_enter();
                        }
                        break;
                        default:
                            break;
                    }
                }
        }
        
        //## statechart_method 
        public int Adjusting_takeEvent(short id) {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            if(event.isTypeOf(RiJEvent.TIMEOUT_EVENT_ID))
                {
                    res = AdjustingTakeRiJTimeout();
                }
            
            if(res == RiJStateReactive.TAKE_EVENT_NOT_CONSUMED)
                {
                    res = Running_takeEvent(id);
                }
            return res;
        }
        
        //## statechart_method 
        public int FinishingTakeRiJTimeout() {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            if(event.getTimeoutId() == Printer_Timeout_Finishing_id)
                {
                    animInstance().notifyTransitionStarted("3");
                    Running_exit();
                    Off_entDef();
                    animInstance().notifyTransitionEnded("3");
                    res = RiJStateReactive.TAKE_EVENT_COMPLETE;
                }
            return res;
        }
        
        //## statechart_method 
        public int PrintingTakeRiJTimeout() {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            if(event.getTimeoutId() == Printer_Timeout_Printing_id)
                {
                    animInstance().notifyTransitionStarted("6");
                    Printing_exit();
                    Finishing_entDef();
                    animInstance().notifyTransitionEnded("6");
                    res = RiJStateReactive.TAKE_EVENT_COMPLETE;
                }
            return res;
        }
        
        //## statechart_method 
        public int Finishing_takeEvent(short id) {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            if(event.isTypeOf(RiJEvent.TIMEOUT_EVENT_ID))
                {
                    res = FinishingTakeRiJTimeout();
                }
            
            if(res == RiJStateReactive.TAKE_EVENT_NOT_CONSUMED)
                {
                    res = Running_takeEvent(id);
                }
            return res;
        }
        
        //## statechart_method 
        public int rootState_takeEvent(short id) {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            return res;
        }
        
        //## statechart_method 
        public void Adjusting_enter() {
            animInstance().notifyStateEntered("ROOT.Running.Adjusting");
            Running_subState = Adjusting;
            rootState_active = Adjusting;
            AdjustingEnter();
        }
        
        //## statechart_method 
        public void Printing_enter() {
            animInstance().notifyStateEntered("ROOT.Running.Printing");
            Running_subState = Printing;
            rootState_active = Printing;
            PrintingEnter();
        }
        
        //## statechart_method 
        public void Running_exit() {
            Running_lastState = Running_subState;
            switch (Running_subState) {
                case Adjusting:
                {
                    Adjusting_exit();
                    Running_lastState = Adjusting;
                }
                break;
                case Printing:
                {
                    Printing_exit();
                    Running_lastState = Printing;
                }
                break;
                case Finishing:
                {
                    Finishing_exit();
                    Running_lastState = Finishing;
                }
                break;
                case LoadFile:
                {
                    LoadFile_exit();
                    Running_lastState = LoadFile;
                }
                break;
                default:
                    break;
            }
            Running_subState = RiJNonState;
            RunningExit();
            animInstance().notifyStateExited("ROOT.Running");
        }
        
        //## statechart_method 
        public int Stop_takeEvent(short id) {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            if(event.isTypeOf(evStart.evStart_Printer_id))
                {
                    res = StopTakeevStart();
                }
            
            return res;
        }
        
        //## statechart_method 
        public int Printing_takeEvent(short id) {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            if(event.isTypeOf(RiJEvent.TIMEOUT_EVENT_ID))
                {
                    res = PrintingTakeRiJTimeout();
                }
            
            if(res == RiJStateReactive.TAKE_EVENT_NOT_CONSUMED)
                {
                    res = Running_takeEvent(id);
                }
            return res;
        }
        
        //## statechart_method 
        public int RunningTakeevStop() {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            animInstance().notifyTransitionStarted("2");
            Running_exit();
            Stop_entDef();
            animInstance().notifyTransitionEnded("2");
            res = RiJStateReactive.TAKE_EVENT_COMPLETE;
            return res;
        }
        
        //## statechart_method 
        public int StopTakeevStart() {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            animInstance().notifyTransitionStarted("5");
            Stop_exit();
            Running_enter();
            Running_entHist();
            animInstance().notifyTransitionEnded("5");
            res = RiJStateReactive.TAKE_EVENT_COMPLETE;
            return res;
        }
        
        //## statechart_method 
        public void Stop_enter() {
            animInstance().notifyStateEntered("ROOT.Stop");
            rootState_subState = Stop;
            rootState_active = Stop;
            StopEnter();
        }
        
        //## statechart_method 
        public void rootState_enter() {
            animInstance().notifyStateEntered("ROOT");
            rootStateEnter();
        }
        
        //## statechart_method 
        public void rootStateEnter() {
        }
        
        //## statechart_method 
        public void Off_entDef() {
            Off_enter();
        }
        
        //## statechart_method 
        public void AdjustingExit() {
            itsRiJThread.unschedTimeout(Printer_Timeout_Adjusting_id, this);
        }
        
        //## statechart_method 
        public void FinishingExit() {
            itsRiJThread.unschedTimeout(Printer_Timeout_Finishing_id, this);
            //#[ state Running.Finishing.(Exit) 
            System.out.print("Finished printing. \nEnter a command:");
            //#]
        }
        
        //## statechart_method 
        public void Finishing_enter() {
            animInstance().notifyStateEntered("ROOT.Running.Finishing");
            Running_subState = Finishing;
            rootState_active = Finishing;
            FinishingEnter();
        }
        
        //## statechart_method 
        public void OffEnter() {
        }
        
        //## statechart_method 
        public void LoadFileEnter() {
            //#[ state Running.LoadFile.(Entry) 
            char[] animationChars = new char[]{'|', '/', '-', '\\'};
            
            for (int i = 0; i <= 100; i++) {
                System.out.print("Load file: " + i + "% " + animationChars[i % 4] + "\r");
            
                try {
                    Thread.sleep(50);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            System.out.print("\n");
            //#]
        }
        
        //## statechart_method 
        public void rootStateEntDef() {
            animInstance().notifyTransitionStarted("0");
            Off_entDef();
            animInstance().notifyTransitionEnded("0");
        }
        
        //## statechart_method 
        public void AdjustingEnter() {
            //#[ state Running.Adjusting.(Entry) 
            System.out.print("Toner Adjustment \n");
            //#]
            itsRiJThread.schedTimeout(adjustTime, Printer_Timeout_Adjusting_id, this, "ROOT.Running.Adjusting");
        }
        
        //## statechart_method 
        public void Finishing_entDef() {
            Finishing_enter();
        }
        
        //## statechart_method 
        public void StopEnter() {
            //#[ state Stop.(Entry) 
            System.out.print("Printing stopped ");
            //#]
        }
        
        //## statechart_method 
        public int OffTakeevPrint() {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            animInstance().notifyTransitionStarted("1");
            Off_exit();
            //#[ transition 1 
            setup();
            //#]
            Running_entDef();
            animInstance().notifyTransitionEnded("1");
            res = RiJStateReactive.TAKE_EVENT_COMPLETE;
            return res;
        }
        
        //## statechart_method 
        public void Off_exit() {
            OffExit();
            animInstance().notifyStateExited("ROOT.Off");
        }
        
        //## statechart_method 
        public void LoadFile_exit() {
            popNullConfig();
            LoadFileExit();
            animInstance().notifyStateExited("ROOT.Running.LoadFile");
        }
        
        //## statechart_method 
        public void PrintingEnter() {
            //#[ state Running.Printing.(Entry) 
            System.out.print("Printing ");
            //#]
            itsRiJThread.schedTimeout(printTime, Printer_Timeout_Printing_id, this, "ROOT.Running.Printing");
        }
        
        //## statechart_method 
        public void Running_enter() {
            animInstance().notifyStateEntered("ROOT.Running");
            rootState_subState = Running;
            RunningEnter();
        }
        
        //## statechart_method 
        public int AdjustingTakeRiJTimeout() {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            if(event.getTimeoutId() == Printer_Timeout_Adjusting_id)
                {
                    animInstance().notifyTransitionStarted("7");
                    Adjusting_exit();
                    Printing_entDef();
                    animInstance().notifyTransitionEnded("7");
                    res = RiJStateReactive.TAKE_EVENT_COMPLETE;
                }
            return res;
        }
        
        //## statechart_method 
        public void rootStateExit() {
        }
        
        //## statechart_method 
        public void OffExit() {
        }
        
        //## statechart_method 
        public void Adjusting_exit() {
            AdjustingExit();
            animInstance().notifyStateExited("ROOT.Running.Adjusting");
        }
        
        //## statechart_method 
        public int LoadFileTakeNull() {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            animInstance().notifyTransitionStarted("8");
            LoadFile_exit();
            Adjusting_entDef();
            animInstance().notifyTransitionEnded("8");
            res = RiJStateReactive.TAKE_EVENT_COMPLETE;
            return res;
        }
        
        //## statechart_method 
        public void Stop_exit() {
            StopExit();
            animInstance().notifyStateExited("ROOT.Stop");
        }
        
        //## statechart_method 
        public int Off_takeEvent(short id) {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            if(event.isTypeOf(evPrint.evPrint_Printer_id))
                {
                    res = OffTakeevPrint();
                }
            
            return res;
        }
        
        //## statechart_method 
        public int LoadFile_takeEvent(short id) {
            int res = RiJStateReactive.TAKE_EVENT_NOT_CONSUMED;
            if(event.isTypeOf(RiJEvent.NULL_EVENT_ID))
                {
                    res = LoadFileTakeNull();
                }
            
            if(res == RiJStateReactive.TAKE_EVENT_NOT_CONSUMED)
                {
                    res = Running_takeEvent(id);
                }
            return res;
        }
        
        //## statechart_method 
        public void LoadFile_enter() {
            animInstance().notifyStateEntered("ROOT.Running.LoadFile");
            pushNullConfig();
            Running_subState = LoadFile;
            rootState_active = LoadFile;
            LoadFileEnter();
        }
        
        //## statechart_method 
        public void Printing_exit() {
            PrintingExit();
            animInstance().notifyStateExited("ROOT.Running.Printing");
        }
        
        /**  methods added just for design level debugging instrumentation */
        public boolean startBehavior() {
            try {
              animInstance().notifyBehavioralMethodEntered("startBehavior",
                  new ArgData[] {
                   });
              return super.startBehavior();
            }
            finally {
              animInstance().notifyMethodExit();
            }
        }
        public int takeEvent(RiJEvent event) { 
            try { 
              //animInstance().notifyTakeEvent(new AnimEvent(event));
              animInstance().notifyBehavioralMethodEntered("takeEvent",
                  new ArgData[] { new ArgData(RiJEvent.class, "event", event.toString())
                   });
              return super.takeEvent(event); 
            }
            finally { 
              animInstance().notifyMethodExit();
            }
        }
        /**  see com.ibm.rational.rhapsody.animation.AnimatedReactive interface */
        public AnimInstance animInstance() { 
            return Printer.this.animInstance(); 
        }
        
    }
    //#[ ignore
    /**  see com.ibm.rational.rhapsody.animation.Animated interface */
    public AnimClass getAnimClass() { 
        return animClassPrinter; 
    }
    /**  see com.ibm.rational.rhapsody.animation.Animated interface */
    public Object getFieldValue(java.lang.reflect.Field f, Object userInstance) { 
         Object obj = null;
         try {
             obj = f.get(userInstance);
         } catch(Exception e) {
              java.lang.System.err.println("Exception: getting Field value: " + e);
              e.printStackTrace();
         }
         return obj;
    }
    /**  see com.ibm.rational.rhapsody.animation.Animated interface */
    public AnimInstance animInstance() {
        if (animate == null) 
            animate = new Animate(); 
        return animate; 
    } 
    /**  see com.ibm.rational.rhapsody.animation.Animated interface */
    public void addAttributes(AnimAttributes msg) {
        
        msg.add("adjustTime", adjustTime);
        msg.add("printTime", printTime);
        msg.add("finishTime", finishTime);
    }
    /**  see com.ibm.rational.rhapsody.animation.Animated interface */
    public void addRelations(AnimRelations msg) {
        
        msg.add("itsDisplay", false, true, itsDisplay);
    }
    /** An inner class added as instrumentation for animation */
    public class Animate extends AnimInstance { 
        public  Animate() { 
            super(Printer.this); 
        } 
        public void addAttributes(AnimAttributes msg) {
            Printer.this.addAttributes(msg);
        }
        public void addRelations(AnimRelations msg) {
            Printer.this.addRelations(msg);
        }
        
        public void addStates(AnimStates msg) {
            if ((reactive != null) && (reactive.isTerminated() == false))
              reactive.rootState_add(msg);
        }
        
    } 
    //#]
    
}
/*********************************************************************
	File Path	: EXE/Host/Printer/Printer.java
*********************************************************************/

