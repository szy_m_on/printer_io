/*********************************************************************
	Rhapsody	: 8.3.1
	Login		: student
	Component	: EXE
	Configuration 	: Host
	Model Element	: evPrint
//!	Generated Date	: Wed, 15, May 2019 
	File Path	: EXE/Host/Printer/evPrint.java
*********************************************************************/

package Printer;

//## auto_generated
import com.ibm.rational.rhapsody.animation.*;
//## auto_generated
import com.ibm.rational.rhapsody.oxf.RiJEvent;
//## auto_generated
import com.ibm.rational.rhapsody.animcom.animMessages.*;

//----------------------------------------------------------------------------
// Printer/evPrint.java                                                                  
//----------------------------------------------------------------------------

//## package Printer 


//## event evPrint() 
public class evPrint extends RiJEvent implements AnimatedEvent {
    
    public static final int evPrint_Printer_id = 21219;		//## ignore 
    
    
    // Constructors
    
    public  evPrint() {
        lId = evPrint_Printer_id;
    }
    
    public boolean isTypeOf(long id) {
        return (evPrint_Printer_id==id);
    }
    
    //#[ ignore
    /** the animated event proxy */
    public static AnimEventClass animClass = new AnimEventClass("Printer.evPrint");
    /**  see com.ibm.rational.rhapsody.animation.AnimatedEvent interface */
    public Object getFieldValue(java.lang.reflect.Field f, Object userInstance) { 
         Object obj = null;
         try {
             obj = f.get(userInstance);
         } catch(Exception e) {
              java.lang.System.err.println("Exception: getting Field value: " + e);
              e.printStackTrace();
         }
         return obj;
    }
    /**  see com.ibm.rational.rhapsody.animation.AnimatedEvent interface */
    public void addAttributes(AnimAttributes msg) {      
    }
    public String toString() {
          String s="evPrint(";      
          s += ")";
          return s;
    }
    //#]
    
}
/*********************************************************************
	File Path	: EXE/Host/Printer/evPrint.java
*********************************************************************/

