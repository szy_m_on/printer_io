/*********************************************************************
	Rhapsody	: 8.3.1
	Login		: student
	Component	: EXE
	Configuration 	: Host
	Model Element	: evStop
//!	Generated Date	: Wed, 15, May 2019 
	File Path	: EXE/Host/Printer/evStop.java
*********************************************************************/

package Printer;

//## auto_generated
import com.ibm.rational.rhapsody.animation.*;
//## auto_generated
import com.ibm.rational.rhapsody.oxf.RiJEvent;
//## auto_generated
import com.ibm.rational.rhapsody.animcom.animMessages.*;

//----------------------------------------------------------------------------
// Printer/evStop.java                                                                  
//----------------------------------------------------------------------------

//## package Printer 


//## event evStop() 
public class evStop extends RiJEvent implements AnimatedEvent {
    
    public static final int evStop_Printer_id = 21217;		//## ignore 
    
    
    // Constructors
    
    public  evStop() {
        lId = evStop_Printer_id;
    }
    
    public boolean isTypeOf(long id) {
        return (evStop_Printer_id==id);
    }
    
    //#[ ignore
    /** the animated event proxy */
    public static AnimEventClass animClass = new AnimEventClass("Printer.evStop");
    /**  see com.ibm.rational.rhapsody.animation.AnimatedEvent interface */
    public Object getFieldValue(java.lang.reflect.Field f, Object userInstance) { 
         Object obj = null;
         try {
             obj = f.get(userInstance);
         } catch(Exception e) {
              java.lang.System.err.println("Exception: getting Field value: " + e);
              e.printStackTrace();
         }
         return obj;
    }
    /**  see com.ibm.rational.rhapsody.animation.AnimatedEvent interface */
    public void addAttributes(AnimAttributes msg) {      
    }
    public String toString() {
          String s="evStop(";      
          s += ")";
          return s;
    }
    //#]
    
}
/*********************************************************************
	File Path	: EXE/Host/Printer/evStop.java
*********************************************************************/

